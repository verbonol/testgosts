﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainScene : MonoBehaviour {

    public Button start = null;
    public Button settings = null;
    public Button restart = null;
    public Button exit = null;
    public Text gameName = null;
    public Text scores = null;
    public Text countScores = null;
    public Text countGosts = null;
    public Image mainImage = null; 
    public int maxCountGosts = 0;
    public ScrollElements scrollElement = null;
    public GameObject gost = null;
    public bool isPause = false;
    bool isStartGame = false;
    public List<GameObject> gosts = new List<GameObject>();

    public int _countGosts = 0;

    private int _scores = 0;
    Coroutine cor = null;


    void Start ()
    {
        countGosts.text = _countGosts + " / " + maxCountGosts;
        scores.enabled = false;
        countGosts.enabled = false;
        countScores.enabled = false;

        start.onClick = StartGame;
        settings.onClick = () =>
        {
            isPause = !isPause;
            scrollElement.isScroll = true;
            if(!isPause && isStartGame)
            {
                StartCoroutine(Spawn());
            }
        };
        restart.onClick = Restart;
        exit.onClick = Exit;

    }
	
	// Update is called once per frame
	void Update ()
    {
       
    }

    private void StartGame()
    {
        isStartGame = true;
        settings.Show();
        start.Hide();
        gameName.enabled = false;
        scores.enabled = true;
        scores.enabled = true;
        countGosts.enabled = true;
        countScores.enabled = true;
        mainImage.enabled = false;
        cor =  StartCoroutine(Spawn());

    }

    private IEnumerator Spawn()
    {
        while(_countGosts <= maxCountGosts && !isPause)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3 (Random.Range(-2.6f, Screen.width - 2.6f), 24f, 1));
            GameObject obj = Instantiate(gost, pos, Quaternion.identity);
            gosts.Add(obj);
            obj.GetComponent<Button>().onClick = () => 
            {
                Destroy(obj);
                _countGosts--;
                countGosts.text = _countGosts + " / " + maxCountGosts;
                countScores.text = (_scores++).ToString();
                gosts.Remove(obj);
            };
            _countGosts++;
            countGosts.text = _countGosts + " / " + maxCountGosts;

            yield return new WaitForSeconds(1.0f);
        }
    }

    private void Restart()
    {
        scrollElement.isScroll = true;
        countScores.text = "0";
        isPause = false;
        _scores = 0;
        foreach (var o in gosts)
        {
            Destroy(o);
        }
        StartGame();
    }

    private void Exit()
    {
        countScores.text = "0";
        isStartGame = false;
        settings.Hide();
        start.Show();
        gameName.enabled = true;
        scores.enabled = false;
        countGosts.enabled = false;
        countScores.enabled = false;
        mainImage.enabled = true;
        scrollElement.isScroll = true;
        isPause = false;
        _scores = 0;
        foreach (var o in gosts)
        {
            Destroy(o);
        }
    }
}
