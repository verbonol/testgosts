﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollElements : MonoBehaviour {

    public float sped = 5f;
    public float checkPos = 0f;
    public bool isScroll = false;
    private RectTransform rect = null; 

	void Start () {
        rect = GetComponent<RectTransform>();
	}
	
	void Update () {
		if(isScroll && rect.offsetMin.y != checkPos)
        {
            rect.offsetMin += new Vector2(rect.offsetMin.x, sped);
            rect.offsetMax += new Vector2(rect.offsetMin.x, sped);
        }
        if(isScroll && rect.offsetMin.y == checkPos)
        {
            isScroll = false;
            checkPos = checkPos == 0 ? - 100f : 0;
            sped = sped == 5 ? -5f : 5f;
        }
	}
}
