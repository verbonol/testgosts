﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gost : MonoBehaviour {

    public float maxSped = 1;
    public float minSped = 1;
    public GameObject obj = null;

    private MainScene _main = null;
    private float sped = 0;

    private RectTransform _rect = null;

    void Start()
    {
        _main = Camera.main.GetComponent<MainScene>();
        _rect = GetComponent<RectTransform>();
        if (maxSped <= 0)
        {
            maxSped = 1f;
        }
        if(minSped <= 0)
        {
            minSped = 1;
        }
        sped = Random.Range(minSped, maxSped);
    }

    void Update()
    {
        if (_rect.offsetMin.y < 10 && !_main.isPause)
        {
            _rect.offsetMin = new Vector2(_rect.offsetMin.x, _rect.offsetMin.y + Time.deltaTime * sped);
        }
        if (_rect.offsetMin.y >= 10)
        {
            _main.gosts.Remove(obj);
            Destroy(obj);
           _main._countGosts--;
        }
    }
}
