﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button : MonoBehaviour {

    public bool isVisible = true;
    public delegate void OnMouseClick();
    public OnMouseClick onClick = null;

    private BoxCollider2D _collider = null;
    private Image _image = null;

    void Start()
    {
        _collider = GetComponent<BoxCollider2D>();
        _image = GetComponent<Image>();

        if (!isVisible)
        {
            Hide();
        }
    }

    void OnMouseDown()
    {
    }

    void OnMouseUp()
    {
        //Debug.Log("Up");
        if(onClick != null)
        {
            onClick();
        }
    }


    public void Show()
    {
        if (this._collider)
        {
            this._collider.enabled = true;
        }
        if (this._image)
        {
            this._image.enabled = true;
        }
    }

    public void Hide()
    {
        if (this._collider)
        {
            this._collider.enabled = false;
        }
        if (this._image)
        {
            this._image.enabled = false;
        }
    }
}
